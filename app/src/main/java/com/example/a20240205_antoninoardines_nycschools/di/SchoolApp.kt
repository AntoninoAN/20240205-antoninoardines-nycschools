package com.example.a20240205_antoninoardines_nycschools.di

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SchoolApp: Application()