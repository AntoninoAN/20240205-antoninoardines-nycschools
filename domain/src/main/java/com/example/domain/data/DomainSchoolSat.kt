package com.example.domain.data

data class DomainSchoolSat(
    val schoolID: String,
    val schoolName: String,
    val satTestTakers: String,
    val satCriticalReading: String,
    val satMath: String,
    val satWriting: String,
    val overview: String,
    val phoneNumber: String,
    val website: String,
    val schoolEmail: String
)
