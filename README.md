
- School NYC Project

-- Architecture: Clean Architecture
-- 3 modules app -> domain -> model
-- Module App
--- It contains the Android Framework. The UI its build using Jetpack Compose and Compose Navigation. There are 3 major composables that are used to build up the UI. 
-- Module Domain
--- Describes the business logic to fetch and store data. It follows the Single source of thruth. There are 2 Use cases that are followed to provide data to the Presentation layer.
-- Module Model
--- It defines the 2 data sources for the project, remote that uses Retrofit and Moshi builder to fetch data from the Network and local that uses Room DB to store the data combined from the School List and the School Sat.

Overral project uses Hilt injection to provide dependencies between the layers.

![Detail Screen](Detail_School_Screen.png)
![List Screen](School_List_Screen.png)
